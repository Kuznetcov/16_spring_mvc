<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="css/style.css" rel="stylesheet">

</head>

<body>
	<div class="wrapper">


		<div class="b-page__line">
			<div class="b-menu">
				<a href="places" class='b-link b-link_menu b-link_menu_active'>HOME</a>
				<a href="#" class='b-link b-link_menu'>ABOUT US</a> <a
					href="agreement.html" class='b-link b-link_menu'>SERVICES</a> <a
					href="#" class='b-link b-link_menu'>CUSTOMERS</a> <a href="#"
					class='b-link b-link_menu'>CONTACT</a>
			</div>
		</div>

		<div class="middle">

			<div class="container" id="1234">
				<main class="content" id="mainClass">
				<div id="popup" style="display: block">

					<div class="info" id="sendform" style="display: block">
						<span class="popup-close" id='close'>x</span>
						<form method="post">
							Name:<br> <input type="text" name="name"
								pattern="[a-zA-Z]{4,}" required><br> Login:<br>
							<input type="text" name="login" pattern="[a-zA-Z]{4,}" required><br>
							Password:<br> <input type="text" name="password"
								pattern="[a-zA-Z]{4,}" required><br> <input
								type="submit" />
						</form>

					</div>
				</div>
			</div>
			<!-- .container-->

		</div>
		<!-- .middle-->

		<div class="footer">Footer</div>
	</div>
	<!-- .wrapper -->

	<script src="js/popup.js" async></script>
	<script src="js/jquery-3.0.0.js"></script>
</body>
</html>