package com.dao.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"ParkingPlace\"")
public class ParkingPlaceDAO implements Serializable{
  /**
   * 
   */
  private static final long serialVersionUID = -3638857580452807131L;
  
  @Id
  @Column(name = "id")
  public Integer placeID;
  @Column(name = "available")
  public Boolean avaliable=true;
  @Column(name = "owner")
  public Integer owner;
  
  
  public synchronized Integer getPlaceID() {
    return placeID;
  }
  public synchronized void setPlaceID(Integer placeID) {
    this.placeID = placeID;
  }
  public synchronized Boolean getAvaliable() {
    return avaliable;
  }
  public synchronized void setAvaliable(Boolean avaliable) {
    this.avaliable = avaliable;
  }
  public synchronized Integer getOwner() {
    return owner;
  }
  public synchronized void setOwner(Integer owner) {
    this.owner = owner;
  }
  
 
  
  
  
}
