package com.dao.data;

import java.io.Serializable;

public class CarDAO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -8151600985366758441L;

  public String model;
  public Integer owner;
  public Integer number;

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public Integer getOwnerID() {
    return owner;
  }

  public void setOwnerID(Integer ownerID) {
    this.owner = ownerID;
  }

  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }


}
