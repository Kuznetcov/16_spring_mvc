package com.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.controller.data.SessionDataBean;
import com.dao.DataBaseDAO;

@Controller
@RequestMapping("/login")
public class LoginForm {

  static Logger log = LoggerFactory.getLogger(LoginForm.class);
  @Autowired
  private DataBaseDAO dataBaseDAO;

  @Autowired
  private SessionDataBean sessionDataBean;

  @RequestMapping(method = RequestMethod.GET)
  public String showLogin(ModelMap model) {
    return "login";
  }

  @RequestMapping(method = RequestMethod.POST)
  public String checkLoginForm(ModelMap model, @RequestParam(value = "login") final String login,
      @RequestParam(value = "password") final String password, HttpServletRequest request,
      HttpServletResponse response) {

    String username = dataBaseDAO.getUser(login, password);


    if (username != null) {
      Cookie cookie = new Cookie("friendlyusername", username);
      sessionDataBean.setLogin(login);
      sessionDataBean.setName(username);
      sessionDataBean.setPassword(password);
      sessionDataBean.setSessionId(request.getSession().getId());
      response.addCookie(cookie);
      model.addAttribute("sessionBean", sessionDataBean);
      log.info("Login accepted: " + login);
      return "redirect:places";
    } else {
      model.addAttribute("error", "User not found");
      return "error";
    }

  }

}
