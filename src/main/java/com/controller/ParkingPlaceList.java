package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.controller.data.SessionDataBean;
import com.dao.DataBaseDAO;
import com.data.ParkingPlace;

@Controller
public class ParkingPlaceList {

  @Autowired
  private DataBaseDAO dataBaseDAO;
  @Autowired
  private SessionDataBean sessionDataBean;

  @RequestMapping(value = "/neverDirectUsedPageToShowParkingPlaces", method = RequestMethod.GET)
  public String showParkingPlaces(ModelMap model,
      @CookieValue(value = "friendlyusername", defaultValue = "%username") String username) {

    List<ParkingPlace> places = dataBaseDAO.getParkingPlaces();
    model.addAttribute("textA", places);
    model.addAttribute("username", username);
    return "places";
  }



  @RequestMapping(value = "/places", method = RequestMethod.GET)
  public String sessionCheck(ModelMap model, HttpServletRequest request) {


     if (sessionDataBean.getSessionId() != null
     && sessionDataBean.getSessionId().equals(request.getSession(false).getId())) {
      return "forward:neverDirectUsedPageToShowParkingPlaces";
    } else {
      model.addAttribute("error", "Session expired");
      return "error";
    }

  }

}
