package com.data;

public class ParkingPlace {

  public Integer placeID;
  public synchronized Integer getPlaceID() {
    return placeID;
  }

  public synchronized void setPlaceID(Integer placeID) {
    this.placeID = placeID;
  }

  public synchronized Boolean getAvaliable() {
    return avaliable;
  }

  public synchronized void setAvaliable(Boolean avaliable) {
    this.avaliable = avaliable;
  }

  public synchronized Integer getOwner() {
    return owner;
  }

  public synchronized void setOwner(Integer owner) {
    this.owner = owner;
  }

  public Boolean avaliable=true;
  public Integer owner;
  
  public ParkingPlace(Integer placeID, Boolean avaliable, Integer owner) {
    super();
    this.placeID = placeID;
    this.avaliable = avaliable;
    this.owner = owner;
  }
 
  
  
  
}
