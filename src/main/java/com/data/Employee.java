package com.data;

public class Employee {

  public String name;
  public Integer id;
  public Integer salary;
  
  public Employee(String name,Integer salary){
    this.name=name;
    this.salary=salary;    
  }

  public synchronized Integer getId() {
    return id;
  }

  public synchronized void setId(Integer id) {
    this.id = id;
  }
  
}
