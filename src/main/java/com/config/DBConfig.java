package com.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.dao.DataBaseDAO;
import com.dao.UseHibernate;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class DBConfig {

//  @Value("${hibernate.connection.driver_class}")
//  private String driverClassName;
//  @Value("${hibernate.connection.url}")
//  private String url;
//  @Value("${hibernate.connection.username}")
//  private String name;
//  @Value("${hibernate.connection.password}")
//  private String password;
//
//  @Value("${hibernate.dialect}")
//  private String dialect;
//  @Value("${hibernate.show_sql}")
//  private String show_sql;
//  @Value("${hibernate.hbm2ddl.auto}")
//  private String hbm2ddl_auto;
//  @Value("${hibernate.temp.use_jdbc_metadata_defaults}")
//  private String metadata_defaults;

//  @Bean
//  public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
//    return new PropertyPlaceholderConfigurer();
//  }

  @Autowired
  Environment env;
  
  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(env.getProperty("hibernate.connection.driver_class"));
    dataSource.setUrl(env.getProperty("hibernate.connection.url"));
    dataSource.setUsername(env.getProperty("hibernate.connection.username"));
    dataSource.setPassword(env.getProperty("hibernate.connection.password"));
    return dataSource;
  }

  @Bean
  public DataBaseDAO dataBaseDAO() {
    return new UseHibernate();
  }


  @Bean
  @Autowired
  public HibernateTemplate getHibernateTemplate(SessionFactory sessionFactory) {
    HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
    hibernateTemplate.setCheckWriteOperations(false);
    return hibernateTemplate;
  }
  
  @Bean
  public Properties getHibernateProperties() {
    Properties properties = new Properties();
    properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
    properties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
    properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
    properties.put("hibernate.temp.use_jdbc_metadata_defaults", env.getProperty("hibernate.temp.use_jdbc_metadata_defaults"));
    return properties;
  }
  
  @Bean
  public LocalSessionFactoryBean getSessionFactory() {
    LocalSessionFactoryBean asfb = new LocalSessionFactoryBean();
    asfb.setDataSource(dataSource());
    asfb.setHibernateProperties(getHibernateProperties());
    asfb.setPackagesToScan(new String[] {"com.dao"});
    return asfb;
  }
  
  @Bean
  @Autowired
  public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
    HibernateTransactionManager htm = new HibernateTransactionManager();
    htm.setSessionFactory(sessionFactory);
    return htm;
  }
  
  @Bean
  public TaskExecutor taskExecutor(){
    return new ThreadPoolTaskExecutor();
  }
  
  
}
